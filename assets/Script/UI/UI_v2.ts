// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import Beat_check from "./Beat_check";
import Heart from "./heart";
@ccclass
export default class UI extends cc.Component {
    public idx = 0;

    @property(Heart)
    heart: Heart = null;

    @property(Beat_check)
    beat_check: Beat_check = null;

    @property(cc.Node)
    coin: cc.Node = null;

    @property(cc.Prefab)
    Beat_bar: cc.Prefab = null;

    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;

    @property(cc.Label)
    missLabel: cc.Label = null;

    @property(cc.Node)
    beat_check_node: cc.Node = null;

    @property(cc.Node)
    heart_node: cc.Node = null;

    private _beatDuration = 0.5;

    private _curr_bgm;

    private _currentBeattime = 2.0;
    // onLoad () {}
    private _bgmname: string = "";

    private _beat_array = null;

    private _get_array = false;

    private idx = 0;
    onLoad() {
        cc.systemEvent.on(
            cc.SystemEvent.EventType.KEY_DOWN,
            this.onKeyDown,
            this
        );
        cc.director.getPhysicsManager().enabled = true;
    }
    start() {
        this.missLabel.node.active = false;
        this.playbgm();
        this.updateHeart(3);
    }

    update(dt) {
        let currentTime = cc.audioEngine.getCurrentTime(this._curr_bgm);
        // if (this._get_array) {
        //     if (currentTime >= this._beat_array[this.idx] / 1000 - 2) {
        //         //console.log(currentTime);
        //         this.generateBeat();
        //         this.idx++;
        //     }

        //     if(cc.audioEngine.getState(this._curr_bgm) == -1){
        //         console.log("end");
                
        //     }
        // }
        // this.node.getChildByName("beat_range").getComponent(cc.RigidBody).syncPosition();
        // //console.log(this.is_onBeat());
    }

    generateBeat() {
        var newBeat_bar = cc.instantiate(this.Beat_bar);
        this.node.addChild(newBeat_bar);
    }

    playbgm() {
        this._curr_bgm = cc.audioEngine.play(this.bgm, false, 0.2);
        //this._bgmname = Global.bgmfile.bgmname;
        //this._beat_array = Global.bgmfile.timearray;
        this._get_array = true;
        console.log(this._beat_array);
    }

    is_onBeat() {
        return true;
    }

    onKeyDown = (event) => {
        console.log(this.is_onBeat());
    };

    is_miss() {
        this.missLabel.node.active = true;
        this.scheduleOnce(function () {
            this.missLabel.node.active = false;
        }, 0.3);
    }

    updateHeart(num: number) {
        this.heart.updateHeart(num);
    }
    // update (dt) {}
}
