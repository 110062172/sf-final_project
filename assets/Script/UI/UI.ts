// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import Beat_check from "./Beat_check";
import Heart from "./heart";
import { Player } from "../Player";

@ccclass
export default class UI extends cc.Component {
    public idx = 0;

    @property(Heart)
    heart: Heart = null;

    @property(Beat_check)
    beat_check: Beat_check = null;


    @property(cc.Prefab)
    Beat_bar: cc.Prefab = null;

    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;

    @property(cc.AudioClip)
    moster_die_sound: cc.AudioClip = null;

    @property(cc.AudioClip)
    player_die_sound: cc.AudioClip = null;

    @property(cc.AudioClip)
    player_attack_sound: cc.AudioClip = null;

    @property(cc.AudioClip)
    player_bow_attack_sound: cc.AudioClip = null;

    @property(cc.AudioClip)
    player_hurt_sound: cc.AudioClip = null;

    @property(cc.Label)
    missLabel: cc.Label = null;

    @property(cc.Node)
    beat_check_node: cc.Node = null;

    @property(cc.Node)
    heart_node: cc.Node = null;

    @property(cc.Node)
    combo_bar_node: cc.Node = null;

    @property(Boolean)
    public is_in_select_scene: boolean = false;

    @property(Boolean)
    is_in_boss_scene: boolean = false;

    @property(cc.Label)
    username_Label: cc.Label = null;

    @property(cc.Label)
    coin_Label: cc.Label = null;

    @property(cc.Label)
    combo_Label: cc.Label = null;
    // @property(cc.Button)
    // pause_button: cc.Button = null;

    @property(cc.Button)
    resume_button: cc.Button = null;

    @property(cc.Button)
    signout_button: cc.Button = null;

    @property(cc.Node)
    pause_ui: cc.Node = null;

    @property(cc.Button)
    boss_btn: cc.Button = null;

    @property(cc.Button)
    dont_die_btn: cc.Button = null;

    @property(cc.Node)
    game_complete_node:cc.Node = null;

    @property(cc.Label)
    game_complete_coin:cc.Label = null;

    @property(cc.Node)
    speaker:cc.Node = null;

    private _beatDuration = 0.5;

    private _curr_bgm;

    private _currentBeattime = 2.0;
    // onLoad () {}
    private _bgmname: string = "";

    private _beat_array = null;

    private _get_array = false;

    private generate_idx:Number = 0, check_idx:Number = 0;


    private is_game_complete:boolean = false;

    onLoad() {
        cc.systemEvent.on(
            cc.SystemEvent.EventType.KEY_DOWN,
            this.onKeyDown,
            this
        );
        cc.director.getPhysicsManager().enabled = true;

        //this.pause_button.node.on("click", this.game_pause, this);
        this.resume_button.node.on("click", this.game_resume, this);
        this.signout_button.node.on("click", this.signout, this);
        this.pause_ui.active = false;
        // this.resume_button.node.active = false;
       
        if(this.boss_btn){
            this.boss_btn.node.on("click", this.load_boss , this);
        }

        if(this.dont_die_btn){
            this.dont_die_btn.node.on("click", this.dont_die , this);
        }

        if(this.game_complete_node){
            this.game_complete_node.active = false;
        }
    }
    start() {
        this.missLabel.node.active = false;
        
        if(!Global.Userdata && !this.is_in_boss_scene) {
            //console.log(Global.Userdata);
            
            Global.Userdata={
                uid:5566,
                username:"test",
                coin:0
            };
            this.updateHeart(5);
        }
        
        if(Global.Userdata.heart){
            cc.find("Player").getComponent(Player).health = Global.Userdata.heart;
            this.updateHeart(Global.Userdata.heart);
        }
        // if (this.is_in_select_scene) {
        //     this.playbgm();
        // }
        if (!this.is_in_select_scene) {
            this.username_Label.node.active = false;
            
        }
        else{
            this.combo_Label.node.active = false;
            this.combo_bar_node.active = false;
            //this.updateHeart(5);
        }

        if(this.is_in_boss_scene){
            this.username_Label.node.active = false;
        }
        //console.log(Global.Userdata);
        this.updatecombo(0);


    }

    update(dt) {
        if(Global.Userdata){
            //console.log(Global.Userdata.heart);
            this.coin_Label.string = Global.Userdata.coin;
            this.username_Label.string = Global.Userdata.username;
        }

        if (!this.is_in_select_scene) {

            if (this._get_array) {
                let currentTime = cc.audioEngine.getCurrentTime(this._curr_bgm);
                if (currentTime >= this._beat_array[this.generate_idx] / 1000 - 2) {
                    //console.log(currentTime);
                    this.generateBeat();
                    this.generate_idx++;
                }
                if(currentTime - 0.2  >= this._beat_array[this.check_idx] / 1000){
                    this.check_idx++;
                }
                if (cc.audioEngine.getState(this._curr_bgm) == -1) {
                    if(!this.is_in_boss_scene){
                        this.load_boss();
                    }
                    else{
                        this.game_complete();
                    }
                    
                    console.log("end");
                }
            }
            this.node
                .getChildByName("beat_range")
                .getComponent(cc.RigidBody)
                .syncPosition(true);
        }
        
        console.log(this.is_onBeat());
    }

    generateBeat() {
        var newBeat_bar = cc.instantiate(this.Beat_bar);
        this.node.addChild(newBeat_bar);
    }

    playbgm() {
        // this._curr_bgm = cc.audioEngine.play(this.bgm, false, 0.2);
        
        if(!this.is_in_boss_scene){
            cc.find("Player").getComponent(Player).health = 5;
        }
        if (!Global.SettingData) {
            Global.SettingData = { sound: 10 };
        }
        cc.audioEngine.setMusicVolume(Global.SettingData.sound / 100);
        cc.audioEngine.setEffectsVolume(Global.SettingData.sound / 100);
        this._curr_bgm = cc.audioEngine.playMusic(
            Global.bgmfile.bgmfile,
            false
        );
        console.log(cc.audioEngine.getMusicVolume());
        if (!this.is_in_select_scene) {
            if(Global.bgmfile){
                if(Global.bgmfile.bgmfile){
                    console.log(Global.bgmfile.bgmfile);
                    
                    this._bgmname = Global.bgmfile.bgmname;
                    this._beat_array = Global.bgmfile.timearray;
                    this._get_array = true;
                    //console.log(this._beat_array);
                }
            }

        }
    }

    is_onBeat() {
        if (this.is_in_select_scene) {
            return true;
        }

        if(this.is_game_complete){
            return false;
        }
        // return this.beat_check.onBeat;
        if(this._get_array){
            let currentTime = cc.audioEngine.getCurrentTime(this._curr_bgm);
            if(currentTime < this._beat_array[0] / 1000){
                return false;
            }


            if(Math.abs(currentTime-0.2 - this._beat_array[this.check_idx] / 1000) < 0.3){
                //this.check_idx++;
                return true;
            }
        }

        return false;
    }

    onKeyDown = (event) => {
        //console.log(this.is_onBeat());
        //this.play_player_hurt_effect();
        //console.log(this.player_hurt_sound);
    };

    is_miss() {
        this.missLabel.node.active = true;
        this.scheduleOnce(function () {
            this.missLabel.node.active = false;
        }, 0.3);
    }

    updateHeart(num: number) {
        if(num <= 0){
            console.log("die");
            
            //this.game_restart();
        }
        if(Global.Userdata){
            Global.Userdata.heart = num;
            this.heart.updateHeart(num);
            
        }
    }

    updatecoin(num: number) {

        if(Global.Userdata){
            if(Global.Userdata.coin){
                Global.Userdata.coin += num;
                this.coin_Label.string = Global.Userdata.coin;
                console.log(Global.Userdata.coin);
                
            }
        }


    }

    updatecombo(num: number) {
        this.combo_Label.string = num;
    }

    player_die(){

    }

    game_pause() {
        //this.resume_button.node.active = true;
        this.pause_ui.active = true;
        this.heart_node.active = false;
        cc.audioEngine.pauseMusic();
        cc.director.pause();
    }

    game_resume() {
        cc.director.resume();
        //this.resume_button.node.active = false;
        cc.audioEngine.resumeMusic();
        this.heart_node.active = true;
        this.pause_ui.active = false;
    }

    game_complete() {
        this.is_game_complete = true;
        this.game_complete_node.active = true;
        if(Global.Userdata){
            Global.Userdata.heart = 5;
            Global.Userdata.coin += parseInt(this.combo_Label.string);
            this.game_complete_coin.string = Global.Userdata.coin;
        }
        else{
            this.game_complete_coin.string = parseInt(this.combo_Label.string);
        }
        let seq = cc.sequence(
            cc.delayTime(5),
            cc.callFunc(() => {
                if(Global.Userdata){
                    var ref = firebase.database().ref("user/" + Global.Userdata.uid);
                    ref.set(Global.Userdata).then(() => {
                        cc.director.loadScene("user_scene");
                    });
                }
                else{
                    cc.director.loadScene("user_scene");
                }
            })
        );
        this.node.runAction(seq);
        this.node.setScale(cc.v2(0, 0));
        if(this.speaker){
            this.speaker.setScale(cc.v2(0, 0));
        }
    }

    game_restart() {
        // cc.director.loadScene("user_scene");
        cc.director.loadScene("main_map1");
    }

    load_level() {
        console.log("main_map1");
        cc.director.loadScene("loading_level_scene");

    }

    load_leaderboard() {
        var ref = firebase.database().ref("user/").orderByChild("coin");
        ref.once("value").then((val) => {
            var data = [];
            val.forEach((element) => {
                data.push([element.val().username, element.val().coin]);
            });
            // console.log(data);
            data.reverse();
            Global.leader = data;
            cc.director.loadScene("leader_board");
        });
    }

    signout(){
        cc.director.resume();
        firebase.auth().signOut().then(() => {
            console.log("signout");
            
            cc.director.loadScene("start_scene");
        });
    }

    load_boss() {
        cc.audioEngine.pauseMusic();
        cc.director.resume();
        cc.director.loadScene("boss room");        
        // let seq = cc.sequence(
        //     cc.delayTime(0.5),
        //     cc.callFunc(() => {
        //         cc.director.loadScene("boss room");
        //     })
        // );
        // this.node.runAction(seq);
    }

    dont_die() {
        
        cc.find("Player").getComponent(Player).health = 100000;
    }


    play_player_hurt_effect() {
        // cc.audioEngine.playEffect(this.player_hurt_sound, false);
        cc.loader.loadRes("/Music/player_hurt_sound", cc.AudioClip, function (err, clip) {
            var audioID = cc.audioEngine.playEffect(clip, false);
        });
    }

    play_moster_die_effect() {
        cc.loader.loadRes("/Music/moster_die_sound", cc.AudioClip, function (err, clip) {
            var audioID = cc.audioEngine.playEffect(clip, false);
        });
    }

    play_player_attack_effect() {
        cc.loader.loadRes("/Music/player_attack_sound", cc.AudioClip, function (err, clip) {
            var audioID = cc.audioEngine.playEffect(clip, false);
        });
    }

    play_player_bow_effect() {
        cc.loader.loadRes("/Music/player_bow_attack_sound", cc.AudioClip, function (err, clip) {
            var audioID = cc.audioEngine.playEffect(clip, false);
        });
    }

    play_player_die_effect() {
        cc.audioEngine.pauseMusic();
        let seq = cc.sequence(
            cc.delayTime(2),
            cc.callFunc(() => {
                cc.director.loadScene("die_scene");
            })
        );
        this.node.runAction(seq);
    }
    // update (dt) {}
}
