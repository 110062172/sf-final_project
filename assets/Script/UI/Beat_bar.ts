// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Beat_bar extends cc.Component {
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    private anim: cc.Animation = null;

    private is_on_destroy = false;

    start() {
        this.anim = this.getComponent(cc.Animation);
        this.anim.play("normal");
        this.anim.on("finished", this.destroyBeatbar, this);
    }

    update(dt) {}

    destroyBeatbar() {
        //console.log(cc.director.getTotalTime() / 1000.0);

        this.scheduleOnce(function () {
            this.node.destroy();
        }, 0.1);
    }

    onBeginContact(contact, self, other) {


        // if(other.node.name == "left" || other.node.name == "right"){
        //     this.onBeat = true;
        // }
    }
    onEndContact(contact, self, other) {
        // if(other.node.name == "left" || other.node.name == "right"){
        //     this.onBeat = false;
        // }
    }
    update(dt) {

    }
}
