// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Beat_check extends cc.Component {
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    public onBeat: boolean = false;

    start() {}

    onBeginContact(contact, self, other) {

        if (other.node.name == "left" || other.node.name == "right") {
            this.onBeat = true;
        }
    }
    onEndContact(contact, self, other) {
        if (other.node.name == "left" || other.node.name == "right") {
            this.onBeat = false;
        }
    }
    update(dt) {}
}
