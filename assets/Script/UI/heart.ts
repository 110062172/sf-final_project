// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Heart extends cc.Component {
    @property(cc.Sprite)
    heart_1: cc.Sprite = null;
    @property(cc.Sprite)
    heart_2: cc.Sprite = null;
    @property(cc.Sprite)
    heart_3: cc.Sprite = null;
    @property(cc.Sprite)
    heart_4: cc.Sprite = null;
    @property(cc.Sprite)
    heart_5: cc.Sprite = null;

    @property(cc.SpriteFrame)
    full_heart: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    half_heart: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    hollow_heart: cc.SpriteFrame = null;
    // onLoad () {}
    
    start() {

    }

    updateHeart(num:number) {
        //console.log(num);
        let heardArr = [this.heart_1, this.heart_2, this.heart_3, this.heart_4, this.heart_5];
        for(var i = 0; i < 5; i++) {
            if(i < num){
                heardArr[i].spriteFrame = this.full_heart;
            }
            else{
                heardArr[i].spriteFrame = this.hollow_heart;
            }
        }
    }
    // update (dt) {}
}
