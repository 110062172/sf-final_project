import { Monster_prototype } from "./monster_prototype";
import { Player } from "./Player";
import { Boss_wall } from "./moving_wall";
import { Char_mng } from "./characters_manager";
import UI from "./UI/UI";

const { ccclass, property } = cc._decorator;

@ccclass
export class Boss extends Monster_prototype {

    @property(Number)
    health: number = 5;

    @property(cc.ParticleSystem)
    blood_effect: cc.ParticleSystem = null;

    @property(Monster_prototype)
    Monster1: Monster_prototype = null;

    @property(Monster_prototype)
    Monster2: Monster_prototype = null;

    @property(Monster_prototype)
    Monster3: Monster_prototype = null;

    @property(cc.Prefab)
    Monster1_prefab: cc.Prefab = null;

    @property(cc.Prefab)
    Monster2_prefab: cc.Prefab = null;

    @property(cc.Prefab)
    Monster3_prefab: cc.Prefab = null;

    @property(Boss_wall)
    left_wall: Boss_wall = null;
    @property(Boss_wall)
    right_wall: Boss_wall = null;

    @property(Char_mng)
    char_mng: Char_mng = null;

    @property(UI)
    ui: UI = null;

    // cycle counter
    private cycle_counter: number = 0;

    // player
    private player: Player = null;

    // moving states
    protected easeRate_moving: number = 2;
    protected block_size: number;
    protected moving_time: number;

    // animation
    protected anim: cc.Animation;
    protected anim_state: cc.AnimationState;

    // moving states
    // state1: random move ; state1: toward player ; state2: teleport preparation ; state3: teleport toward player(2 blocks) ; state4: stall 
    // state1: random move ; state1: toward player ; state2: teleport preparation ; state3: teleport toward player(2 blocks) ; state4: stall ; state5: stall ; state6: random move
    // state7: summon monster2
    private state = 0;
    private num_states = 8;

    private previous_move = -1 // 0: top ; 1: down ; 2: left ; 3: right

    // wall detection

    private wall_on_right: boolean = false;
    private wall_on_left: boolean = false;
    private wall_on_top: boolean = false;
    private wall_on_down: boolean = false;

    // teleport related
    private max_dist_to_teleport: number = 300;

    // random move result
    private random_move_result: number = -1;
    private have_obtained_random_result: boolean = false;

    // left col and go
    private left_col_x: number;

    // middle's y
    private mid_y: number;

    // has pushed
    private has_push_left_wall: boolean = false;
    private has_push_right_wall: boolean = false;

    // summon every 21 cycles
    private summon_counter: number = 0;

    // LIFE-CYCLE CALLBACKS:

    public self_destroy = () => {
        this.anim.stop();
        this.anim_state = this.anim.play("boss_die");
    
        this.anim.on("finished", () => {
            this.node.destroy();
        }, this);


        this.ui.game_complete();
    }

    play_idle = (): cc.AnimationState => {
        return this.anim.play("boss_flying");
    }

    private play_cast = () => {
        this.anim.stop();
        this.anim_state = this.anim.play("boss_cast");
    }

    public play_attack = () => {
        this.anim.stop();
        this.anim_state = this.anim.play("boss_attack");
    }

    push_left_wall = () => {

        if (this.has_push_left_wall) return;

        this.left_wall.go_right();
        this.has_push_left_wall = true;
    }

    push_right_wall = () => {

        if (this.has_push_right_wall) return;

        this.right_wall.go_left();
        this.has_push_right_wall = true;
    }

    go_up = () => {

        let action = cc.moveBy(this.moving_time, 0, this.block_size).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
    }

    go_down = () => {
        let action = cc.moveBy(this.moving_time, 0, -this.block_size).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
    }

    go_left = () => {

        let action = cc.moveBy(this.moving_time, -this.block_size, 0).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);

    }

    go_right = () => {

        let action = cc.moveBy(this.moving_time, this.block_size, 0).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
    }

    public next_pos_toward_player = (): cc.Vec2 => {

        let moving_direction = this.next_pos_direction();

        if (moving_direction == 3) {
            // right
            return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x + this.block_size, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
        }
        else if (moving_direction == 0) {
            // up
            return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y + this.block_size);
        }
        else if (moving_direction == 2) {
            // left
            return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x - this.block_size, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
        }
        else if (moving_direction == 1) {
            // down
            return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y - this.block_size);
        }
        else {
            // stall
            return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
        }
    }

    public move_toward_player = () => {

        let moving_direction = this.next_pos_direction();

        // If the shortest path toward the player is blocked by a wall, then move to a side without a wall
        if (moving_direction == 3) {
            this.go_right();
        }
        else if (moving_direction == 0) {
            this.go_up();
        }
        else if (moving_direction == 2) {
            this.go_left();
        }
        else if (moving_direction == 1) {
            this.go_down();
        }
        else {
            // no where to go
        }
    }

    random_move_direction = (): number => {
        /*
        0 ~ 3
        0: top
        1: down
        2: left
        3: right
         */

        if (this.wall_on_down && this.wall_on_left && this.wall_on_right && this.wall_on_top) {
            return -1; // -1 means stall
        }

        while (true) {
            let random_number: number = Math.floor(Math.random() * 4);
            if (random_number == 0 && !this.wall_on_top) {
                return random_number;
            }
            else if (random_number == 1 && !this.wall_on_down) {
                return random_number;
            }
            else if (random_number == 2 && !this.wall_on_left) {
                return random_number;
            }
            else if (random_number == 3 && !this.wall_on_right) {
                return random_number;
            }
        }
    }

    random_move = (direction: number): boolean => {

        /* direction
        -1: stall
        0: top
        1: down
        2: left
        3: right
        */

        if (direction == -1) {
            return false;
        }

        if (direction == 0) {
            this.go_up();
        }
        else if (direction == 1) {
            this.go_down();
        }
        else if (direction == 2) {
            this.go_left();
        }
        else if (direction == 3) {
            this.go_right();
        }
    }

    // play_blood_effect = () => {
    //     this.blood_effect.resetSystem();
    // }

    random_move_pos = (direction: number): cc.Vec2 => {
        if (direction == -1) {
            return this.current_pos();
        }

        let current_position = this.current_pos();

        if (direction == 0) {
            return cc.v2(current_position.x, current_position.y + this.block_size);
        }
        else if (direction == 1) {
            return cc.v2(current_position.x, current_position.y - this.block_size);
        }
        else if (direction == 2) {
            return cc.v2(current_position.x - this.block_size, current_position.y);
        }
        else if (direction == 3) {
            return cc.v2(current_position.x + this.block_size, current_position.y);
        }
    }

    public get_health = () => {
        return this.health;
    }

    public current_pos = () => {
        return new cc.Vec2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
    }

    public next_pos = () => {

        if (this.state == 0 || this.state == 6) {
            this.random_move_result = this.random_move_direction();
            return this.random_move_pos(this.random_move_result);
        }
        else if (this.state == 2) {
            return this.teleport_destination();
        }
        if (this.state == 3) {

            return this.next_pos_toward_player();
        }

        return this.current_pos();
    }

    public will_move_in_next_state = () => {

        let new_pos = this.next_pos_toward_player();
        let current_pos = this.current_pos();

        if (this.wall_on_down && this.wall_on_left && this.wall_on_right && this.wall_on_top) {
            return false;
        }
        else if (this.state == 2) {
            return true;
        }
        else if (this.state == 3 && new_pos != current_pos) {
            return true;
        }
        else {
            return false;
        }
    }

    public next_pos_direction = () => {

        // -1: stall ; 0: top ; 1: down ; 2: left ; 3: right

        let player_location = this.player.get_position();
        let delta_x = this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x - player_location.x;
        let delta_y = this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y - player_location.y;

        //console.log(player_location.x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, player_location.y, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);

        let can_go_down: boolean = false;
        let can_go_up: boolean = false;
        let can_go_right: boolean = false;
        let can_go_left: boolean = false;

        if (delta_y >= this.block_size * 9 / 10) {
            //can_go_down = this.wall_on_down || this.previous_move == 0 ? false : true;
            can_go_down = this.wall_on_down ? false : true;
        }

        if (delta_y <= -this.block_size * 9 / 10) {
            can_go_up = this.wall_on_top ? false : true;
        }

        if (delta_x >= this.block_size * 9 / 10) {
            can_go_left = this.wall_on_left ? false : true;
        }

        if (delta_x <= -this.block_size * 9 / 10) {
            can_go_right = this.wall_on_right ? false : true;
        }

        if (can_go_right) {
            return 3;
        }
        else if (can_go_left) {
            return 2;
        }
        else if (can_go_up) {
            return 0;
        }
        else if (can_go_down) {
            return 1;
        }

        // If the shortest path toward the player is blocked by a wall, then move to a side without a wall
        if (!this.wall_on_right) {
            return 3;
        }
        else if (!this.wall_on_top) {
            return 0;
        }
        else if (!this.wall_on_left) {
            return 2;
        }
        else if (!this.wall_on_down) {
            return 1;
        }
        else {
            // no where to go
            return -1;
        }
    }

    public distance = (v1: cc.Vec2, v2: cc.Vec2) => {
        return Math.sqrt((v1.x - v2.x) ** 2 + (v1.y - v2.y) ** 2);
    }

    public teleport_destination = (): cc.Vec2 => {

        if (this.distance(this.player.get_position(), cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y)) > this.max_dist_to_teleport) {
            return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
        }

        if (!this.player.has_wall_on_top()) {
            let player_pos = this.player.get_position();
            return cc.v2(player_pos.x, player_pos.y + this.block_size);
        }
        else if (!this.player.has_wall_on_bottom()) {
            let player_pos = this.player.get_position();
            return cc.v2(player_pos.x, player_pos.y - this.block_size);
        }
        else if (!this.player.has_wall_on_left()) {
            let player_pos = this.player.get_position();
            return cc.v2(player_pos.x - this.block_size, player_pos.y);
        }
        else if (!this.player.has_wall_on_right()) {
            let player_pos = this.player.get_position();
            return cc.v2(player_pos.x + this.block_size, player_pos.y);
        }

        return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
    }

    public move = () => {

        if (this.state == 0) {
            // this.random_move_result would be set by next_pos since next_pos is alway called before this function(move) 
            this.random_move(this.random_move_result);
        }
        else if (this.state == 1) {
            this.play_teleport_preparation();
        }
        else if (this.state == 2) {
            // this.move_toward_player();
            // this.move_toward_player();

            let destination = this.teleport_destination();
            this.node.setPosition(destination.x - this.node.getParent().x, destination.y - this.node.getParent().y);
            this.start_teleport();
        }
        else if (this.state == 3) {
            this.move_toward_player();
        }
        else if (this.state == 6) {
            this.random_move(this.random_move_result);
        }
        else if (this.state == 7) {

            if (this.summon_counter >= 23) {
                this.summon_counter = 0;
                this.play_cast();
                this.summon_monster2();
            }
        }
        else {
            // stall
        }

        // change state
        ++this.summon_counter;
        this.state = (this.state + 1) % this.num_states;
        this.cycle_counter++;

        // if (this.cycle_counter == 20) {
        //     if (this.player.has_wall_on_left()) {
        //         --this.cycle_counter;
        //     }
        //     else {
        //         this.push_left_wall();
        //         this.left_col_x += this.block_size;
        //     }

        // }
        // else if (this.cycle_counter == 40) {

        //     if (this.player.has_wall_on_right()) {
        //         --this.cycle_counter;
        //     }
        //     else {
        //         this.push_right_wall();
        //     }
        // }
    }

    summon_monster2 = () => {

        let num_cols_inside_box = 8;
        // we have 11 - 2 - 1 available cols

        while (true) {
            let random_number: number = Math.floor(Math.random() * num_cols_inside_box);
            if (Math.abs(this.player.get_position().x - (this.left_col_x + random_number * this.block_size)) < this.block_size / 2) {
                // overlap
            }
            else {
                console.log(random_number);
                this.instantiate_monster2(this.left_col_x + random_number * this.block_size, this.mid_y);
                break;
            }
        }
    }

    instantiate_monster2 = (global_x, global_y) => {

        let random_y_offset = Math.floor(Math.random() * 3) - 1;

        let monster2_node = cc.instantiate(this.Monster2_prefab);

        this.node.parent.addChild(monster2_node);
        monster2_node.setPosition(cc.v2(global_x - this.node.parent.x, global_y + (random_y_offset * this.block_size) - this.node.parent.y));
    }

    onBeginContact(contact, self, other) {
        // if (other.tag == 6) {
        //     this.attacked();
        // }
    }

    onPreSolve(contact, self, other) {
        //console.log(Math.abs(self.node.x - other.node.x), Math.abs(self.node.y - other.node.y));
        // if (other.tag == 5) {
        //     console.log("monster encountered.");
        // }

        let delta_x = self.node.convertToWorldSpaceAR(cc.v2(0, 0)).x - other.node.convertToWorldSpaceAR(cc.v2(0, 0)).x;
        let delta_y = self.node.convertToWorldSpaceAR(cc.v2(0, 0)).y - other.node.convertToWorldSpaceAR(cc.v2(0, 0)).y;
        //console.log(delta_x, delta_y, other.tag, this.block_size / 10);
        // start to contact a wall
        if (other.tag == 11 || other.tag == 5) {
            if (delta_y >= 0 && delta_y <= this.block_size * 3 / 2 &&
                delta_x >= -this.block_size / 10 && delta_x <= this.block_size / 10) {
                // wall block is right below the player
                this.wall_on_down = true;
                //console.log("down");
            }
            else if (delta_y <= 0 && delta_y >= -this.block_size * 3 / 2 &&
                delta_x >= -this.block_size / 10 && delta_x <= this.block_size / 10) {
                // wall block is right above the player
                this.wall_on_top = true;
                //console.log("top");
            }
            else if (delta_x >= 0 && delta_x <= this.block_size * 3 / 2 &&
                delta_y >= -this.block_size / 10 && delta_y <= this.block_size / 10) {
                // wall block is on the left-hand side of the player
                this.wall_on_left = true;
            }
            else if (delta_x <= 0 && delta_x >= -this.block_size * 3 / 2 &&
                delta_y >= -this.block_size / 10 && delta_y <= this.block_size / 10) {
                // wall block is on the right-hand side of the player
                this.wall_on_right = true;
                //console.log("test123");
            }
        }
    }

    onEndContact(contact, self, other) {
        // if (other.tag == 5) {
        //     console.log("away from monster.");
        // }
        this.wall_on_down = this.wall_on_left = this.wall_on_right = this.wall_on_top = false;
    }

    play_blood_effect = () => {
        this.blood_effect.resetSystem();
    }

    play_teleport_preparation = () => {
        this.anim.stop();
        this.anim_state = this.anim.play("boss_ready_teleport");
    }

    start_teleport = () => {
        this.anim.stop();
        this.anim_state = this.anim.play("boss_teleport");
    }

    public attacked = () => {
        this.play_blood_effect();
        --this.health;

        if (this.health <= 0) {
            this.self_destroy();
        }
    }

    onLoad() {
        // obtain block size

        //this.block_size = cc.find("Player").getComponent(Player).get_block_size();

        // animation
        this.anim = this.getComponent(cc.Animation);
        this.anim_state = this.play_idle();
        this.anim_state.repeatCount = Infinity;

        // obtain block size
        this.player = cc.find("Player").getComponent(Player);
        this.block_size = this.player.get_block_size();
        this.moving_time = this.player.get_moving_time();

        if (this.char_mng == null) {
            console.log("char_mng not found.");
        }

        this.left_col_x = this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x;
        this.mid_y = this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y;

        //this.summon_monster2();

        // load ui
        this.ui = cc.find("Canvas/Main Camera/UI").getComponent(UI);
    }

    start() {

    }

    update(dt) {

        // updata animation
        if (!this.anim_state.isPlaying) {
            //this.anim_state = this.anim.play('player_idle');
            this.anim_state = this.anim.play("boss_flying");
            this.anim_state.repeatCount = Infinity;
        }
    }
}
