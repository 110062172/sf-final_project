const { ccclass, property } = cc._decorator;

const ROOM_SIZE = 640;
const trimSize = 1;
const dirx = [0, 0, 1, -1];
const diry = [1, -1, 0, 0];

@ccclass
export class game extends cc.Component {
    @property(cc.Node)
    mapNode: cc.Node = null;
    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getCollisionManager().enabled = true;
    }
    start()
    {
        cc.loader.loadRes('Map/lobby', cc.TiledMapAsset, (err,assets) => {
            let node=new cc.Node();
            let map:cc.TiledMap=node.addComponent(cc.TiledMap);
            map.tmxAsset=assets;
            this.node.addChild(node);
        });
    }
}