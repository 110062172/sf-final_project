import { Monster_prototype } from "./monster_prototype";
import { Player } from "./Player";

const { ccclass, property } = cc._decorator;

@ccclass
export class Monster3 extends Monster_prototype {

    @property(Number)
    health: number = 2;

    @property(cc.ParticleSystem)
    blood_effect: cc.ParticleSystem = null;

    // moving states
    // state0: up ; state1: stall ; state2: down ; state3: stall
    private state = 0;
    private num_states = 2;
    protected easeRate_moving: number = 2;
    protected block_size: number;
    protected moving_time: number;
    private previous_move = -1 // 0: top ; 1: down ; 2: left ; 3: right

    // wall detection

    private wall_on_right: boolean = false;
    private wall_on_left: boolean = false;
    private wall_on_top: boolean = false;
    private wall_on_down: boolean = false;

    // player
    private player: Player = null;

    // animation
    protected anim: cc.Animation;
    protected anim_state: cc.AnimationState;

    // LIFE-CYCLE CALLBACKS:

    public self_destroy = () => {
        this.anim.stop();
        this.anim_state = this.anim.play("bat_die");
        this.anim.on("finished", () => {
            this.node.destroy();
        }, this);
    }

    play_blood_effect = () => {
        this.blood_effect.resetSystem();
    }

    play_idle = (): cc.AnimationState => {
        return this.anim.play("bat_idle");
    }

    public play_attack = () => {

    }

    public get_health = () => {
        return this.health;
    }

    public current_pos = () => {
        return new cc.Vec2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
    }

    public out_of_scale = (): boolean => {

        if (this.distance(this.player.get_position(), this.current_pos()) >= this.block_size * 6) return true;
        return false;
    }

    public next_pos = () => {

        if (this.out_of_scale()) {
            return this.current_pos();
        }

        if (this.state == 1) {

            return this.next_pos_toward_player();
        }

        return this.current_pos();
    }

    public will_move_in_next_state = () => {

        if (this.out_of_scale()) {

            return false;
        }

        let new_pos = this.next_pos_toward_player();
        let current_pos = this.current_pos();

        if (this.state == 1 && new_pos != current_pos) {
            return true;
        }
        else {
            return false;
        }
    }

    public attacked = () => {
        this.play_blood_effect();
        --this.health;

        if (this.health <= 0) {
            this.self_destroy();
        }
    }

    public distance = (v1: cc.Vec2, v2: cc.Vec2) => {
        return Math.sqrt((v1.x - v2.x) ** 2 + (v1.y - v2.y) ** 2);
    }

    public move = () => {

        if (this.out_of_scale()) {
            return;
        }

        if (this.state == 0) {
            // do nothing
        }
        else if (this.state == 1) {
            // approach the player
            this.move_toward_player();
        }

        // change state
        this.state = (this.state + 1) % this.num_states;
        //console.log("state", this.state);
    }

    public next_pos_direction = () => {

        // -1: stall ; 0: top ; 1: down ; 2: left ; 3: right

        let player_location = this.player.get_position();
        let delta_x = this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x - player_location.x;
        let delta_y = this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y - player_location.y;

        //console.log(player_location.x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, player_location.y, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);

        let can_go_down: boolean = false;
        let can_go_up: boolean = false;
        let can_go_right: boolean = false;
        let can_go_left: boolean = false;

        if (delta_y >= this.block_size * 9 / 10) {
            can_go_down = this.wall_on_down || this.previous_move == 0 ? false : true;
        }

        if (delta_y <= -this.block_size * 9 / 10) {
            can_go_up = this.wall_on_top || this.previous_move == 1 ? false : true;
        }

        if (delta_x >= this.block_size * 9 / 10) {
            can_go_left = this.wall_on_left || this.previous_move == 3 ? false : true;
        }

        if (delta_x <= -this.block_size * 9 / 10) {
            can_go_right = this.wall_on_right || this.previous_move == 2 ? false : true;
        }

        if (can_go_right) {
            return 3;
        }
        else if (can_go_left) {
            return 2;
        }
        else if (can_go_up) {
            return 0;
        }
        else if (can_go_down) {
            return 1;
        }

        // If the shortest path toward the player is blocked by a wall, then move to a side without a wall
        if (!this.wall_on_right && this.previous_move != 2) {
            return 3;
        }
        else if (!this.wall_on_top && this.previous_move != 1) {
            return 0;
        }
        else if (!this.wall_on_left && this.previous_move != 3) {
            return 2;
        }
        else if (!this.wall_on_down && this.previous_move != 0) {
            return 1;
        }
        else {
            // no where to go
            return -1;
        }
    }

    public next_pos_toward_player = (): cc.Vec2 => {

        let moving_direction = this.next_pos_direction();

        if (moving_direction == 3) {
            // right
            return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x + this.block_size, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
        }
        else if (moving_direction == 0) {
            // up
            return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y + this.block_size);
        }
        else if (moving_direction == 2) {
            // left
            return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x - this.block_size, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
        }
        else if (moving_direction == 1) {
            // down
            return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y - this.block_size);
        }
        else {
            // stall
            return cc.v2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
        }
    }

    public move_toward_player = () => {

        let moving_direction = this.next_pos_direction();

        // If the shortest path toward the player is blocked by a wall, then move to a side without a wall
        if (moving_direction == 3) {
            this.go_right();
        }
        else if (moving_direction == 0) {
            this.go_up();
        }
        else if (moving_direction == 2) {
            this.go_left();
        }
        else if (moving_direction == 1) {
            this.go_down();
        }
        else {
            // no where to go
        }
    }

    go_up = () => {

        let action = cc.moveBy(this.moving_time, 0, this.block_size).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
        this.previous_move = 0;
    }

    go_down = () => {
        let action = cc.moveBy(this.moving_time, 0, -this.block_size).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
        this.previous_move = 1;
    }

    go_left = () => {

        let action = cc.moveBy(this.moving_time, -this.block_size, 0).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);

        if (this.node.scaleX < 0) {
            this.node.scaleX *= -1;
        }

        this.previous_move = 2;
    }

    go_right = () => {

        let action = cc.moveBy(this.moving_time, this.block_size, 0).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);

        if (this.node.scaleX > 0) {
            this.node.scaleX *= -1;
        }

        this.previous_move = 3;
    }

    onBeginContact(contact, self, other) {
        // if (other.tag == 6) {
        //     this.attacked();
        // }
    }

    onPreSolve(contact, self, other) {
        //console.log(Math.abs(self.node.x - other.node.x), Math.abs(self.node.y - other.node.y));
        // if (other.tag == 5) {
        //     console.log("monster encountered.");
        // }

        let delta_x = self.node.convertToWorldSpaceAR(cc.v2(0, 0)).x - other.node.convertToWorldSpaceAR(cc.v2(0, 0)).x;
        let delta_y = self.node.convertToWorldSpaceAR(cc.v2(0, 0)).y - other.node.convertToWorldSpaceAR(cc.v2(0, 0)).y;
        //console.log(delta_x, delta_y, other.tag, this.block_size / 10);
        // start to contact a wall
        if (other.tag == 10) {
            this.wall_on_left = true;
        }
        else if (other.tag == 9) {
            this.wall_on_right = true;
        }
        else if (other.tag == 8) {
            this.wall_on_top = true;
        }
        else if (other.tag == 7) {
            this.wall_on_down = true;
        }
        else if (other.tag == 11 || other.tag == 5) {
            if (delta_y >= 0 && delta_y <= this.block_size * 3 / 2 &&
                delta_x >= -this.block_size / 10 && delta_x <= this.block_size / 10) {
                // wall block is right below the player
                this.wall_on_down = true;
                //console.log("down");
            }
            else if (delta_y <= 0 && delta_y >= -this.block_size * 3 / 2 &&
                delta_x >= -this.block_size / 10 && delta_x <= this.block_size / 10) {
                // wall block is right above the player
                this.wall_on_top = true;
                //console.log("top");
            }
            else if (delta_x >= 0 && delta_x <= this.block_size * 3 / 2 &&
                delta_y >= -this.block_size / 10 && delta_y <= this.block_size / 10) {
                // wall block is on the left-hand side of the player
                this.wall_on_left = true;
            }
            else if (delta_x <= 0 && delta_x >= -this.block_size * 3 / 2 &&
                delta_y >= -this.block_size / 10 && delta_y <= this.block_size / 10) {
                // wall block is on the right-hand side of the player
                this.wall_on_right = true;
                //console.log("test123");
            }
        }
    }

    onEndContact(contact, self, other) {
        // if (other.tag == 5) {
        //     console.log("away from monster.");
        // }
        this.wall_on_down = this.wall_on_left = this.wall_on_right = this.wall_on_top = false;
    }

    onLoad() {
        // obtain block size
        this.player = cc.find("Player").getComponent(Player);
        this.block_size = this.player.get_block_size();
        this.moving_time = this.player.get_moving_time();

        // animation
        this.anim = this.getComponent(cc.Animation);
        this.anim_state = this.play_idle();
        this.anim_state.repeatCount = Infinity;
    }

    start() {
    }

    update(dt) {
    }
}
