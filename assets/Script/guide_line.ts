

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.ParticleSystem)
    flame: cc.ParticleSystem = null;

    // animation
    protected anim: cc.Animation;
    protected anim_state: cc.AnimationState;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

        // paritcle 

        this.getComponent(cc.ParticleSystem);

        // animation
        this.anim = this.getComponent(cc.Animation);
        this.anim_state = this.anim.play();
        this.anim_state.repeatCount = Infinity;
    }

    start() {

    }

    update(dt) {
        // if (this.flame.active == false) {
        //     this.flame.resetSystem();
        // }

        //console.log(this.anim_state);

    }
}
