
const { ccclass, property } = cc._decorator;

@ccclass
export abstract class Monster_prototype extends cc.Component {

    @property(Number)
    health: number;
    protected block_size: number;

    // animation
    protected anim: cc.Animation;
    protected anim_state: cc.AnimationState;

    public abstract play_idle(): cc.AnimationState;
    public abstract play_attack();
    public abstract get_health(): number;
    public abstract current_pos(): cc.Vec2;
    // pos. in the next period
    public abstract next_pos(): cc.Vec2;
    // will move in the next period
    public abstract will_move_in_next_state(): boolean;
    // being attack
    public abstract attacked();
    // moving method
    public abstract move();

    // destroy itself
    public abstract self_destroy();

    abstract onBeginContact(contact, self, other);

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

    }

    // update (dt) {}
}
