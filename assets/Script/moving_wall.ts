const { ccclass, property } = cc._decorator;

@ccclass
export class Boss_wall extends cc.Component {

    @property(cc.Prefab)
    boss_wall: cc.Prefab = null;

    @property(Boss_wall)
    new_wall: Boss_wall = null;

    private moving_time = .3;
    private block_size = 64;
    private easeRate_moving = 2;
    // LIFE-CYCLE CALLBACKS:

    go_up = () => {

        let action = cc.moveBy(this.moving_time, 0, this.block_size).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
    }

    go_down = () => {
        let action = cc.moveBy(this.moving_time, 0, -this.block_size).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
    }

    go_left = () => {

        let action = cc.moveBy(this.moving_time, -this.block_size, 0).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
    }

    go_right = () => {

        let action = cc.moveBy(this.moving_time, this.block_size, 0).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
    }

    instantiate_new_wall = () => {
        let wall = cc.instantiate(this.boss_wall);
        this.node.parent.addChild(wall);
        wall.setPosition(this.node.position);
        this.new_wall = wall.getComponent(Boss_wall);
    }

    onLoad() {
    }


    start() {
        console.log(this.node.children);
    }

    // update (dt) {}
}
