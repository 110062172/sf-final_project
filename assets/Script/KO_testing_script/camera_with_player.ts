import { Player } from "../Player";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    private player: Player = null;
    private previous_player_pos: cc.Vec2;

    // LIFE-CYCLE CALLBACKS:

    update_camera_pos = () => {
        let player_pos = this.player.get_position();
        this.node.x = this.node.x + player_pos.x - this.previous_player_pos.x;
        this.node.y = this.node.y + player_pos.y - this.previous_player_pos.y;

        this.previous_player_pos = player_pos;
    }

    onLoad() {
        this.player = cc.find("Player").getComponent(Player);
        this.previous_player_pos = this.player.get_position();
    }

    start() {

    }

    update(dt) {
        this.update_camera_pos();
    }
}
