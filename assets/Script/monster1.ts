import { Monster_prototype } from "./monster_prototype";
import { Player } from "./Player";

const { ccclass, property } = cc._decorator;

@ccclass
export class Monster1 extends Monster_prototype {

    @property(Number)
    health: number = 1;

    @property(cc.ParticleSystem)
    blood_effect: cc.ParticleSystem = null;

    // moving states
    protected block_size: number;

    // animation
    protected anim: cc.Animation;
    protected anim_state: cc.AnimationState;

    // LIFE-CYCLE CALLBACKS:

    play_idle = (): cc.AnimationState => {
        return this.anim.play("yellow_slime_idle");
    }

    public play_attack = () => {

    }

    play_blood_effect = () => {
        this.blood_effect.resetSystem();
    }

    public self_destroy = () => {
        this.getComponent(cc.PhysicsBoxCollider).tag = 6;
        this.anim.stop();
        this.anim_state = this.anim.play("yellow_slime_die");
        this.anim.on("finished", () => {
            this.node.destroy();
        }, this);
    }

    public get_health = () => {
        return this.health;
    }

    public current_pos = () => {
        return new cc.Vec2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
    }

    public next_pos = () => {
        return new cc.Vec2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
    }

    public will_move_in_next_state = () => {
        return false;
    }

    public attacked = () => {
        this.play_blood_effect();
        --this.health;

        if (this.health <= 0) {
            this.self_destroy();
        }
    }

    public move = () => {

    }

    onBeginContact(contact, self, other) {
        // if (other.tag == 6) {
        //     this.attacked();
        // }
    }

    onLoad() {
        // obtain block size

        //this.block_size = cc.find("Player").getComponent(Player).get_block_size();

        // animation
        this.anim = this.getComponent(cc.Animation);
        this.anim_state = this.play_idle();
        this.anim_state.repeatCount = Infinity;
    }

    start() {

    }

    update(dt) {
    }
}
