import { Player } from "./Player";
import { Monster_prototype } from "./monster_prototype";
import UI from "./UI/UI";

const { ccclass, property } = cc._decorator;

@ccclass
export class Char_mng extends cc.Component {

    @property(cc.Component)
    player: cc.Component = null;

    @property(cc.Component)

    private ui: UI = null;
    private children: cc.Node[] = null;
    private block_size: number;

    //updating control
    /*
    Beat:  off   -> on   -> off
    pivot: false -> true -> false

    when on -> off, we can change pivot from true to false. i.e.
    state change from (beat: off, pivot: true) to (beat: off, pivot: false)

    when beat first turn off, then update_state 

    use done_this_turn to record if move_detection called before we want to update
    */

    private canUpdate: boolean = true;
    private pivot: boolean = false;
    private done_this_turn: boolean = false;

    private testing_timer = 0;

    // LIFE-CYCLE CALLBACKS:

    public distance = (v1: cc.Vec2, v2: cc.Vec2) => {
        return Math.sqrt((v1.x - v2.x) ** 2 + (v1.y - v2.y) ** 2);
    }

    player_attack_monster = (monster_component: Monster_prototype) => {

        if (monster_component.get_health() == 1) {

            if (monster_component.node.name == "Boss") {
                this.ui.updatecoin(50);
            }
            else {
                this.ui.updatecoin(1); 
            }

            this.ui.play_moster_die_effect();
        }

        monster_component.attacked();
        this.player.getComponent(Player).play_normal_attack_animation();
    }

    move_detection = (origin_vec, new_vec) => {
        // return true iff player can move 

        // let it won't update state as turning off
        this.done_this_turn = true;

        // selection of attacking
        let selection_target: number = -1;
        let min_dist: number = -1;

        // detection of monsters

        for (var i = 0; i < this.children.length; ++i) {
            if (this.children[i].getComponent(Monster_prototype).get_health() <= 0) {
                continue;
            }

            let monster_component: Monster_prototype = this.children[i].getComponent(Monster_prototype);
            let monster_next_pos: cc.Vec2 = monster_component.next_pos();
            let monster_current_pos: cc.Vec2 = monster_component.current_pos();

            let player_Player_component = this.player.getComponent(Player);
            let player_weapon_state: number = player_Player_component.get_weapon_state();

            // if(going to overlap), then...
            //console.log(new_vec.x, monster_next_pos.x);
            if (player_weapon_state == 1 && Math.abs(origin_vec.x - monster_next_pos.x) > this.block_size * 11 / 10 && Math.abs(origin_vec.x - monster_next_pos.x) < this.block_size * 21 / 10 &&
                Math.abs(new_vec.y - monster_next_pos.y) < this.block_size / 10) {

                player_Player_component.correction_of_scaleX(origin_vec, new_vec); // makes player's direction correct

                if (new_vec.x - origin_vec.x > 0 && origin_vec.x < monster_next_pos.x) {

                    player_Player_component.shoot_arrow(3);
                    console.log("shoot right");

                    monster_component.move();
                    this.player.getComponent(Player).play_normal_attack_animation();
                    this.player_attack_monster(monster_component);

                    return false;
                }
                else if (new_vec.x - origin_vec.x < 0 && origin_vec.x > monster_next_pos.x) {
                    player_Player_component.shoot_arrow(2);
                    console.log("shoot left");

                    monster_component.move();
                    this.player.getComponent(Player).play_normal_attack_animation();
                    this.player_attack_monster(monster_component);

                    return false;
                }

                monster_component.move();
                return true;
            }
            else if (player_weapon_state == 1 && Math.abs(origin_vec.y - monster_next_pos.y) > this.block_size * 11 / 10 && Math.abs(origin_vec.y - monster_next_pos.y) < this.block_size * 21 / 10 &&
                Math.abs(new_vec.x - monster_next_pos.x) < this.block_size / 10) {

                if (new_vec.y - origin_vec.y > 0 && origin_vec.y < monster_next_pos.y) {
                    player_Player_component.shoot_arrow(0);
                    console.log("shoot top");

                    monster_component.move();
                    this.player.getComponent(Player).play_normal_attack_animation();
                    this.player_attack_monster(monster_component);

                    return false;
                }
                else if (new_vec.y - origin_vec.y < 0 && origin_vec.y > monster_next_pos.y) {
                    player_Player_component.shoot_arrow(1);
                    console.log("shoot bot");

                    monster_component.move();
                    this.player.getComponent(Player).play_normal_attack_animation();
                    this.player_attack_monster(monster_component);

                    return false;
                }

                monster_component.move();
                return true;
            }
            else if (Math.abs(new_vec.x - monster_next_pos.x) < this.block_size / 5 && Math.abs(new_vec.y - monster_next_pos.y) < this.block_size / 5) {

                console.log("monster encountered health: ", monster_component.get_health());
                // console.log(new_vec, monster_component.current_pos(), monster_next_pos);

                // console.log(monster_component.will_move_in_next_state());

                // if monster is not going to move , then its health decreases
                // else if monster is going to move and has only 1 health, then health decreases to 0 and die
                // else: player get hurt
                if (!monster_component.will_move_in_next_state()) {
                    //monster_component.attacked();
                    player_Player_component.correction_of_scaleX(origin_vec, new_vec); // makes player's direction correct
                    this.player_attack_monster(monster_component);
                }
                else if (monster_component.get_health() <= 1) {
                    //monster_component.attacked(); // and it will die soon
                    player_Player_component.correction_of_scaleX(origin_vec, new_vec); // makes player's direction correct
                    this.player_attack_monster(monster_component);
                }
                else {
                    // player is attacked 
                    this.children[i].getComponent(Monster_prototype).play_attack();
                    this.player.getComponent(Player).attacked();
                }

                if (monster_component.get_health() > 0) {
                    // if the monster is not going to move than player can not move to the spot
                    if (monster_component.will_move_in_next_state()) {

                        return true;
                    }
                    else {
                        //monster_component.move();
                        return false;
                    }
                }
                else {
                    if (monster_component.will_move_in_next_state()) {
                        return true;
                    }
                    else {
                        //monster_component.move();
                        return false;
                    }
                    //break;
                }
            }
            else if (Math.abs(origin_vec.x - monster_next_pos.x) < this.block_size / 2 && Math.abs(origin_vec.y - monster_next_pos.y) < this.block_size / 2 &&
                Math.abs(new_vec.x - monster_current_pos.x) < this.block_size / 2 && Math.abs(new_vec.y - monster_current_pos.y) < this.block_size / 2) {

                if (monster_component.get_health() <= 1) {
                    //monster_component.attacked(); // and it will die soon
                    player_Player_component.correction_of_scaleX(origin_vec, new_vec); // makes player's direction correct
                    this.player_attack_monster(monster_component);
                }
                else {
                    // player is attacked 
                    this.children[i].getComponent(Monster_prototype).play_attack();
                    this.player.getComponent(Player).attacked();
                    this.player_attack_monster(monster_component);
                }

                if (monster_component.get_health() > 0) {
                    // can not move
                    return false;
                }
                else {
                    return false;
                    //break;
                }
            }
            else {
                monster_component.move();
            }
        }

        // if (eliminated_index != -1) {
        //     this.node.children[i].destroy();
        //     this.children = this.node.children;
        //     console.log("eliminated");
        // }

        return true;
    }

    test_onBeat = () => {
        // if (this.testing_timer > 0.6) {
        //     this.testing_timer -= .5;

        //     return true;
        // }
        // else {
        //     return false;
        // }
        return this.ui.is_onBeat();
    }

    update_state = () => {

        // get current player pos.
        let player_pos: cc.Vec2 = new cc.Vec2(this.player.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.player.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);

        for (var i = 0; i < this.children.length; ++i) {
            if (this.children[i].getComponent(Monster_prototype).get_health() <= 0) {
                continue;
            }

            let monster_component: Monster_prototype = this.children[i].getComponent(Monster_prototype);
            let monster_next_pos: cc.Vec2 = monster_component.next_pos();

            //console.log(player_pos.y, monster_next_pos.y);
            if (Math.abs(player_pos.x - monster_next_pos.x) < this.block_size * 7 / 10 && Math.abs(player_pos.y - monster_next_pos.y) < this.block_size * 7 / 10) {
                console.log("player is attacked.");
                this.children[i].getComponent(Monster_prototype).play_attack();
                this.player.getComponent(Player).attacked();
            }
            else {
                this.children[i].getComponent(Monster_prototype).move();
            }
        }
    }

    onLoad() {
        this.children = this.node.children;

        this.block_size = this.player.getComponent(Player).get_block_size();

        // load ui
        this.ui = cc.find("Canvas/Main Camera/UI").getComponent(UI);
    }

    start() {

    }

    update(dt) {
        this.testing_timer += dt;

        if (this.test_onBeat()) {
            // on

            // check if it is the first time to turn on in this period
            // if it is, change pivot to true

            if (this.pivot == false) {
                this.pivot = true;
            }
        }
        else {
            // off

            //when on -> off, we can change pivot from true to false. i.e.
            //state change from (beat: off, pivot: true, canUpdate: false) to (beat: off, pivot: false, canUpdate: true) and wait for the next update

            if (this.pivot == true) {
                this.pivot = false;

                if (!this.done_this_turn) {
                    this.canUpdate = true;
                }
                else {
                    // reset done_this_turn for the next period
                    this.done_this_turn = false;
                }
            }

            if (this.canUpdate) {
                this.canUpdate = false;

                this.update_state();
                console.log("updated");
            }
        }
    }
}
