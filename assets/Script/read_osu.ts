// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
import UI from "./UI/UI";
const { ccclass, property } = cc._decorator;

window.Global = {
    bgmfile: null,
};

@ccclass
export default class OsuReader extends cc.Component {
    // LIFE-CYCLE CALLBACKS:
    public bgmfile: cc.AudioClip = null;
    public timearray: Array = null;
    public bgmname: string = null;

    onLoad() {
        // cc.resources.loadDir("/osu/", function (err, file: TextAsset) {
        //     if (err) {
        //         console.log(err.message);
        //     } else {
        //         //console.log(file);
        //         let bgm_name = "";
        //         for (var i = 0; i < file.length; i++) {
        //             let newfile = file[i]._nativeAsset.toString();
        //             let file_array = newfile.split("\r\n");
        //             for (var j = 0; j < file_array.length; j++) {
        //                 if (file_array[j] == "[General]") {
        //                     j++;
        //                     bgm_name = file_array[j].substring(15);
        //                     this.get_bgm(bgm_name);
        //                 }

        //                 if (file_array[j] == "[HitObjects]") {
        //                     j++;
        //                     var timearray = [];
        //                     while (
        //                         j < file_array.length &&
        //                         file_array[j] != ""
        //                     ) {
        //                         let HitObjects = file_array[j].split(",");

        //                         if (
        //                             timearray.length == 0 ||
        //                             (timearray.length > 0 &&
        //                                 HitObjects[2] !=
        //                                     timearray[timearray.length - 1])
        //                         ) {
        //                             timearray.push(HitObjects[2]);
        //                         }

        //                         j++;
        //                     }
        //                     Global.bgmfile = {
        //                         bgmname: bgm_name,
        //                         timearray: timearray,
        //                     };
        //                     console.log(Global.bgmfile);
        //                 }
        //             }
        //         }
        //         //cc.find("Canvas/Main Camera/UI").getComponent(UI).playbgm();
        //     }
        // });
        var osu_array = [];
        var ref = firebase.database().ref("file/osu");
        ref.once("value", (res) => {
            res.forEach((child) => {
                osu_array.push(child.val());
            });
            //console.log(osu_array);
            this.get_file("osu", osu_array[Math.floor(Math.random() * osu_array.length)]);
        });
    }

    start() {
        //path = "../osu/1-3osu"
    }

    get_file(type, filename) {
        let progressCallback = (
            completeCount: number,
            totalCount: number,
            item: any
        ) => {
            //console.log("download progress: ", completeCount, totalCount, item);
        };

        let completeCallback = (error: Error, res: any) => {
            //console.log("completeCallback", error, res);
            if (res) {
                //console.log(res);
                if (type == "osu") {
                    this.read_osu_file(res);
                } else {
                    console.log(res);
                    this.bgmfile = res;
                    Global.bgmfile = {
                        bgmfile: this.bgmfile,
                        bgmname: this.bgmname,
                        timearray: this.timearray,
                    };
                    //console.log(Global.bgmfile);
                    cc.find("Canvas/Main Camera/UI").getComponent(UI).playbgm();
                }
            }
        };
        cc.loader.load(
            "http://localhost:3000/api/v1/download/" + filename,
            progressCallback,
            completeCallback
        );
    }

    read_osu_file(newfile) {
        let file_array = newfile.split("\r\n");
        for (var j = 0; j < file_array.length; j++) {
            if (file_array[j] == "[General]") {
                j++;
                this.bgmname = file_array[j].substring(15);
                this.get_file("bgm", this.bgmname);
            }

            if (file_array[j] == "[HitObjects]") {
                j++;
                var new_timearray = [];
                while (j < file_array.length && file_array[j] != "") {
                    let HitObjects = file_array[j].split(",");

                    if (
                        new_timearray.length == 0 ||
                        (new_timearray.length > 0 &&
                            HitObjects[2] !=
                                new_timearray[new_timearray.length - 1])
                    ) {
                        new_timearray.push(HitObjects[2]);
                    }

                    j++;
                }
                this.timearray = new_timearray;
            }
        }
    }
    // update (dt) {}
}
