import { Char_mng } from "./characters_manager";
import { Arrow } from "./arrow";
import UI from "./UI/UI";

const { ccclass, property } = cc._decorator;

@ccclass
export class Player extends cc.Component {

    @property(cc.Component)
    characters_mng_sprite: cc.Component = null;

    @property(Char_mng)
    characters_mng: Char_mng = null;

    @property(cc.ParticleSystem)
    blood_effect: cc.ParticleSystem = null;

    @property(cc.Prefab)
    arrow_prefab: cc.Prefab = null;

    private up: boolean = false;
    private down: boolean = false;
    private left: boolean = false;
    private right: boolean = false;
    private wall_on_right: boolean = false;
    private wall_on_left: boolean = false;
    private wall_on_top: boolean = false;
    private wall_on_down: boolean = false;
    private easeRate_moving: number = 2;
    private moving_time: number = .3;
    private block_size = 64;
    private anim: cc.Animation;
    private anim_state: cc.AnimationState;
    private testing_timer = 0;
    private move_lock: boolean = false;
    private move_previous: booleain = false;
    public health: number = 5;
    private ui: UI = null;

    /* weapon state
    0: bare hand (default)
    1: bow
    */

    // combo counter
    private combo_counter: number = 0;

    private weapon_state: number = 0;

    // LIFE-CYCLE CALLBACKS:

    get_moving_time = () => {
        return this.moving_time;
    }

    get_block_size = () => {
        return this.block_size;
    }

    get_weapon_state = (): number => {
        return this.weapon_state;
    }

    get_position = () => {
        // pos. in global coordinate
        return new cc.Vec2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
    }

    public has_wall_on_top = () => {
        return this.wall_on_top;
    }

    public has_wall_on_bottom = () => {
        return this.wall_on_down;
    }

    public has_wall_on_left = () => {
        return this.wall_on_left;
    }

    public has_wall_on_right = () => {
        return this.wall_on_right;
    }

    correction_of_scaleX = (origin_vec: cc.Vec2, new_vec: cc.Vec2) => {
        // 1: right ; -1: left
        let direction = (new_vec.x - origin_vec.x > 0) ? 1 : -1;

        if (direction == 1 && this.node.scaleX > 0) {
            this.node.scaleX *= -1;
        }
        else if (direction == -1 && this.node.scale < 0) {
            this.node.scaleX *= -1;
        }
    }

    play_normal_attack_animation = () => {
        this.anim.stop();

        // play normal attack with corresponding weapon
        //this.anim_state = this.anim.play("player_normal_attack");
        this.anim_state = this.anim.play(this.anim.getClips()[this.weapon_state * 2 + 1].name);
        if (this.weapon_state == 0) {
            this.ui.play_player_attack_effect();
        }
        else if (this.weapon_state == 1) {
            this.ui.play_player_bow_effect();
        }
    }

    play_die_animation = () => {
        this.anim.stop();
        this.anim_state = this.anim.play("player_die");
    }

    shoot_arrow = (direction: Number) => {
        let new_arrow = cc.instantiate(this.arrow_prefab);
        let arrow = new_arrow.getComponent(Arrow);
        this.node.addChild(new_arrow);

        new_arrow.setPosition(0, 0);

        // 0: up ; 1: down ; 2: left ; 3: right
        if (direction == 0) {
            arrow.go_up();
        }
        else if (direction == 1) {
            arrow.go_down();
        }
        else if (direction == 2) {
            arrow.go_left();
        }
        else if (direction == 3) {
            arrow.go_right();
        }


    }

    play_blood_effect = () => {
        this.blood_effect.resetSystem();
    }

    change_to_bow_state = () => {
        this.anim.stop();

        this.weapon_state = 1;

        this.anim_state = this.anim.play(this.anim.getClips()[this.weapon_state * 2].name);
        this.anim_state.repeatCount = Infinity;
    }

    go_up = () => {

        let action = cc.moveBy(this.moving_time, 0, this.block_size).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
        this.up = false;
    }

    go_down = () => {
        let action = cc.moveBy(this.moving_time, 0, -this.block_size).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
        this.down = false;
    }

    go_left = () => {

        let action = cc.moveBy(this.moving_time, -this.block_size, 0).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);

        if (this.node.scaleX < 0) {
            this.node.scaleX *= -1;
        }

        this.left = false;
    }

    go_right = () => {

        let action = cc.moveBy(this.moving_time, this.block_size, 0).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);

        if (this.node.scaleX > 0) {
            this.node.scaleX *= -1;
        }

        this.right = false;
    }

    attacked = () => {

        if (this.health <= 0) return;

        --this.health;
        console.log("player's remaining health: ", this.health);

        this.play_blood_effect();

        if (this.health <= 0) {
            this.play_die_animation();
            this.ui.play_player_die_effect();
        }
        else {
            this.ui.play_player_hurt_effect();
        }

        this.ui.updateHeart(this.health);
    }

    move = () => {

        let current_pos = this.get_position();

        if (this.right == true) {

            if (this.wall_on_right) {
                this.right = false;
                return;
            }

            let can_move: boolean = this.characters_mng.move_detection(new cc.Vec2(current_pos.x, current_pos.y), new cc.Vec2(current_pos.x + this.block_size, current_pos.y));
            if (can_move) {
                this.go_right();
            }
            else {
                this.right = false;
            }
        }
        else if (this.left == true) {

            if (this.wall_on_left == true) {
                this.left = false;
                return;
            }

            let can_move: boolean = this.characters_mng.move_detection(new cc.Vec2(current_pos.x, current_pos.y), new cc.Vec2(current_pos.x - this.block_size, current_pos.y));
            if (can_move) {
                this.go_left();
            }
            else {
                this.left = false;
            }
        }
        else if (this.up == true) {

            if (this.wall_on_top == true) {
                this.up = false;
                return;
            }

            let can_move: boolean = this.characters_mng.move_detection(new cc.Vec2(current_pos.x, current_pos.y), new cc.Vec2(current_pos.x, current_pos.y + this.block_size));
            if (can_move) {
                this.go_up();
            }
            else {
                this.up = false;
            }
        }
        else if (this.down == true) {

            if (this.wall_on_down == true) {
                this.down = false;
                return;
            }

            let can_move: boolean = this.characters_mng.move_detection(new cc.Vec2(current_pos.x, current_pos.y), new cc.Vec2(current_pos.x, current_pos.y - this.block_size));
            if (can_move) {
                this.go_down();
            }
            else {
                this.down = false;
            }
        }

        // this.wall_on_down = this.wall_on_left = this.wall_on_right = this.wall_on_top = false;
    }

    onKeyDown = (event) => {

        if (this.health <= 0) return;

        if (!this.ui.is_in_select_scene) {
            if (!this.test_onBeat()) {
                console.log("fail to move due to wrong tempo");
                // reset counter
                this.combo_counter = 0;
                this.ui.updatecombo(this.combo_counter);

                this.ui.getComponent(UI).is_miss();
                // reset lock for the next tempo
                this.move_lock = false;
                return;
            }
            else if (this.move_lock) {
                return;
            }
            else {
                // ++counter 
                ++this.combo_counter;
                this.ui.updatecombo(this.combo_counter);
                console.log(this.combo_counter);


                // this.test_onBeat is true and lock is false
                // set lock to be true
                this.move_previous = true;
                this.move_lock = true;
            }
        }

        if (event.keyCode == cc.macro.KEY.escape) {
            this.ui.game_pause();
        }
        else if (event.keyCode == cc.macro.KEY.d) {
            this.right = true;
            this.left = false;
            this.up = false;
            this.down = false;
        }
        else if (event.keyCode == cc.macro.KEY.a) {
            this.right = false;
            this.left = true;
            this.up = false;
            this.down = false;
        }
        else if (event.keyCode == cc.macro.KEY.w) {
            this.right = false;
            this.left = false;
            this.up = true;
            this.down = false;
        }
        else if (event.keyCode == cc.macro.KEY.s) {
            this.right = false;
            this.left = false;
            this.up = false;
            this.down = true;
        }
    }

    onKeyUp = (event) => {
        if (event.keyCode == cc.macro.KEY.d) {
            this.right = false;
        }
        else if (event.keyCode == cc.macro.KEY.a) {
            this.left = false;
        }
        else if (event.keyCode == cc.macro.KEY.w) {
            this.up = false;
        }
        else if (event.keyCode == cc.macro.KEY.s) {
            this.down = false;
        }
    }

    test_onBeat = () => {
        // if (this.testing_timer >= 0.4 && this.testing_timer <= .6) {
        //     this.testing_timer -= .5;

        //     return true;
        // }
        // else {
        //     return false;
        // }


        return this.ui.is_onBeat();
    }

    onPreSolve(contact, self, other) {
        //console.log(Math.abs(self.node.x - other.node.x), Math.abs(self.node.y - other.node.y));
        // if (other.tag == 5) {
        //     console.log("monster encountered.");
        // }


        if (other.tag == 13) {
            console.log("load leader_board from player");
            this.ui.load_leaderboard();
            return;
        }
        else if (other.tag == 12) {
            console.log("load level from player");
            this.ui.load_level();
        }
        else if (other.tag == 66) {
            console.log("load boss' room from player");
            this.ui.load_boss();
        }

        let delta_x = self.node.convertToWorldSpaceAR(cc.v2(0, 0)).x - other.node.convertToWorldSpaceAR(cc.v2(0, 0)).x;
        let delta_y = self.node.convertToWorldSpaceAR(cc.v2(0, 0)).y - other.node.convertToWorldSpaceAR(cc.v2(0, 0)).y;
        //console.log(delta_x, delta_y, other.tag, this.block_size / 10);

        // start to contact a wall(11) or weapon(12)
        if (other.tag == 10) {
            this.wall_on_left = true;
        }
        else if (other.tag == 9) {
            this.wall_on_right = true;
        }
        else if (other.tag == 8) {
            this.wall_on_top = true;
        }
        else if (other.tag == 7) {
            this.wall_on_down = true;
        }
        else if (other.tag == 11) {
            if (delta_y >= 0 && delta_y <= this.block_size * 11 / 10 &&
                delta_x >= -this.block_size / 10 && delta_x <= this.block_size / 10) {
                // wall block is right below the player
                this.wall_on_down = true;
                //console.log("down");
            }
            else if (delta_y <= 0 && delta_y >= -this.block_size * 11 / 10 &&
                delta_x >= -this.block_size / 10 && delta_x <= this.block_size / 10) {
                // wall block is right above the player
                this.wall_on_top = true;
                //console.log("top");
            }
            else if (delta_x >= 0 && delta_x <= this.block_size * 11 / 10 &&
                delta_y >= -this.block_size / 10 && delta_y <= this.block_size / 10) {
                // wall block is on the left-hand side of the player
                this.wall_on_left = true;
            }
            else if (delta_x <= 0 && delta_x >= -this.block_size * 11 / 10 &&
                delta_y >= -this.block_size / 10 && delta_y <= this.block_size / 10) {
                // wall block is on the right-hand side of the player
                this.wall_on_right = true;
                //console.log("test123");
            }
        }
        else if (other.tag == 20 && Math.abs(delta_x) < this.block_size / 2 && Math.abs(delta_y) < this.block_size / 2) {
            this.change_to_bow_state();
            other.node.destroy();
        }

        //console.log(this.wall_on_top, this.wall_on_down, this.wall_on_left, this.wall_on_right);
    }

    onEndContact(contact, self, other) {
        // if (other.tag == 5) {
        //     console.log("away from monster.");
        // }
        this.wall_on_down = this.wall_on_left = this.wall_on_right = this.wall_on_top = false;
    }

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;

        // movement
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

        // animation
        this.anim = this.getComponent(cc.Animation);
        this.anim_state = this.anim.play(this.anim.getClips()[this.weapon_state * 2].name);
        this.anim_state.repeatCount = Infinity;

        // load mng
        this.characters_mng = this.characters_mng_sprite.getComponent(Char_mng);

        // load ui
        this.ui = cc.find("Canvas/Main Camera/UI").getComponent(UI);
        // this.health = Global.Userdata.heart;
        //console.log(this.ui);


    }

    start() {

    }

    update(dt) {
        //console.log(this.anim.getClips()[1].name);
        if(!this.move_previous && !this.test_onBeat() && !this.move_lock){
            this.combo_counter = 0;
            this.ui.updatecombo(0);
        }

        if(this.test_onBeat() && this.move_lock == false){
            this.move_previous = false;
        }

        if (!this.test_onBeat()) {
            
            this.move_lock = false;
        }
        // testing_timer is just used for test
        this.testing_timer += dt;


        // updata animation
        if (!this.anim_state.isPlaying && this.health > 0) {
            //this.anim_state = this.anim.play('player_idle');
            this.anim_state = this.anim.play(this.anim.getClips()[this.weapon_state * 2].name);
            this.anim_state.repeatCount = Infinity;
        }
        /////////////////////////////

        this.move();

        if (this.testing_timer > .6) {
            this.testing_timer -= .5;
        }
    }
}
