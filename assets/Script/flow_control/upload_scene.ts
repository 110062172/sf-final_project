// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class upload extends cc.Component {
    @property(cc.Button)
    back_btn: cc.Button = null;

    @property(cc.Button)
    music_btn: cc.Button = null;

    @property(cc.Button)
    osu_btn: cc.Button = null;

    // LIFE-CYCLE CALLBACKS:
    public input_music_element = null;
    public input_osu_element = null;

    onLoad() {
        //this.back_btn.node.on("click", this.loaduserScene, this);
        this.music_btn.node.on("click", this.upload_music, this);
        this.osu_btn.node.on("click", this.upload_osu, this);



        this.input_music_element: HTMLInputElement = <HTMLInputElement>(
            document.getElementById("input_music_element")
        );

        if (!this.input_music_element) {
            this.input_music_element = document.createElement("input");
            this.input_music_element.id = "input_music_element";
            this.input_music_element.setAttribute("id", "input_music_element");
            this.input_music_element.setAttribute("type", "file");
            this.input_music_element.setAttribute("class", "fileToUpload");
            this.input_music_element.style.opacity = 0;
            this.input_music_element.style.position = "absolute";
            document.body.appendChild(this.input_music_element);
        }
        this.input_music_element.onchange = (event: Event) => {
            let files = this.input_music_element.files;
            if (files && files.length > 0) {
                var file = files[0];
                this.upload_file_to_firebase("music", file);
            }
        };

        this.input_osu_element: HTMLInputElement = <HTMLInputElement>(
            document.getElementById("input_osu_element")
        );

        if (!this.input_osu_element) {
            this.input_osu_element = document.createElement("input");
            this.input_osu_element.id = "input_osu_element";
            this.input_osu_element.setAttribute("id", "input_osu_element");
            this.input_osu_element.setAttribute("type", "file");
            this.input_osu_element.setAttribute("class", "fileToUpload");
            this.input_osu_element.style.opacity = 0;
            this.input_osu_element.style.position = "absolute";
            document.body.appendChild(this.input_osu_element);
        }
        this.input_osu_element.onchange = (event: Event) => {
            let files = this.input_osu_element.files;
            if (files && files.length > 0) {
                var file = files[0];
                this.upload_file_to_firebase("osu", file);
            }
        };
    }

    start() {}

    loaduserScene() {
        //console.log("user");

    }

    upload_music() {
        this.input_music_element.click();
        console.log("click upload_music");
        
    }

    upload_osu() {
        this.input_osu_element.click();
        console.log("click upload_osu");
    }

    upload_file_to_firebase(type ,file) {

        // cc.sys.localStorage.setItem("music", JSON.stringify(file));
        let from = new FormData();
        from.append("file", file);
        from.append("filename", file.name);
        from.append("targetPath", "/" + type);
        let xhr = cc.loader.getXMLHttpRequest();
        xhr.onreadystatechange = function(){
            if(xhr.readyState == 4){
                if(xhr.status >= 200 && xhr.status <= 300 || xhr.status == 304){
                    console.log(xhr.response);
                    var ref = firebase.database().ref("file/" + type + "/");
                    ref.push(file.name).then(() => {
                        console.log("upload file");
                    });
                }
                else{
                    console.log(xhr.status);
                }
            }
        }

        xhr.open("POST", "http://localhost:3000/api/v1/upload", true);
        xhr.addEventListener("progress", function(evt){
            console.log("progress", evt);
            
        },false);
        xhr.send(from);
    }


    // update (dt) {}
}
