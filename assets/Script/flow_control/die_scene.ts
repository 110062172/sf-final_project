// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Button)
    back_button: cc.Button = null;

    @property(cc.Button)
    respawn_button: cc.Button = null;

    @property(cc.Node)
    button_node: cc.Node = null;



    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        if (!Global.SettingData) {
            Global.SettingData = { sound: 10 };
        }
        cc.audioEngine.setMusicVolume(Global.SettingData.sound / 100);
        cc.audioEngine.setEffectsVolume(Global.SettingData.sound / 100);

        this.back_button.node.on("click", this.back_user_scene, this);
        this.respawn_button.node.on("click", this.respawn, this);

    }

    start () {

        

 
        cc.loader.loadRes("/Music/player_die_sound", cc.AudioClip, function (err, clip) {
            var audioID = cc.audioEngine.playEffect(clip, false);
        });
    }

    back_user_scene(){
        Global.Userdata.heart = 5;
        cc.director.loadScene("user_scene");
    }

    respawn(){
        if(!Global.Userdata){
            Global.Userdata ={
                uid:5566,
                username:"aaa",
                coin:0,
                heart:5
            }
        }
        Global.Userdata.heart = 5;
        cc.director.loadScene("main_map1");
    }
    // update (dt) {}
}
