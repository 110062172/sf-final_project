// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

window.Global = {
    SettingData: null,
};
@ccclass
export default class start extends cc.Component {
    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;

    @property(cc.Button)
    start_btn: cc.Button = null;

    @property(cc.Button)
    setting_btn: cc.Button = null;

    public SettingData = null;
    onLoad() {
        this.start_btn.node.on("click", this.loadSigninScene, this);
        this.setting_btn.node.on("click", this.loadSettingScene, this);
        this.SettingData = JSON.parse(
            cc.sys.localStorage.getItem("SettingData")
        );
    }
    start() {
        //cc.audioEngine.playMusic(this.bgm, true);
        // let offline_btn = new cc.Component.EventHandler();
        // offline_btn.target = this.node;
        // offline_btn.component = "start";
        // offline_btn.handler = "loadOfflineScene";
        // cc.find("Canvas/offline_btn")
        //     .getComponent(cc.Button)
        //     .clickEvents.push(offline_btn);

        // var ref = firebase.database().ref("user/");
        // ref.on("value", (val) => {
        //     console.log(val.val());
        // });

        if (!this.SettingData) {
            var newSetting = {
                sound: 20,
            };
            cc.sys.localStorage.setItem(
                "SettingData",
                JSON.stringify(newSetting)
            );
            Global.SettingData = newSetting;
            cc.audioEngine.setMusicVolume(0.2);
        } else {
            Global.SettingData = this.SettingData;
            cc.audioEngine.setMusicVolume(this.SettingData.sound / 100);
        }

        console.log(Global.SettingData);
        cc.audioEngine.playMusic(this.bgm, true);
    }
    loadSigninScene() {
        cc.director.loadScene("sign_in");
    }

    loadSettingScene() {
        cc.director.loadScene("setting_scene");
    }

    // loadOfflineScene() {
    //     Global.offline = true;
    //     var userData = JSON.parse(cc.sys.localStorage.getItem("userData"));
    //     if (userData) {
    //         cc.director.loadScene("loading");
    //     } else {
    //         cc.director.loadScene("offline");
    //     }
    // }

    // update (dt) {}
}
