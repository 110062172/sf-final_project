// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class setting extends cc.Component {
    @property(cc.Button)
    save_btn: cc.Button = null;

    @property(cc.Slider)
    sound_slider: cc.Slider = null;

    @property(cc.Label)
    sound_label: cc.Label = null;



    public sound_value: int = null;
    // var userData = {
    //     coin: 0,
    //     score: 0,
    //     max_score: 0,
    //     complete1: false,
    //     life: 5,
    //     username: Global.username,
    // };
    // cc.sys.localStorage.setItem(
    //     "userData",
    //     JSON.stringify(userData)
    // );
    onLoad() {

        this.save_btn.node.on("click", this.saveSetting, this);
        this.sound_slider.node.on("slide", this.change_sound_slider, this);
    }

    start() {
        if(!Global.SettingData){
            this.sound_slider.progress = 0.5;
        }
        else{
            this.sound_slider.progress = Global.SettingData.sound / 100;
            
        }
        this.sound_value = parseInt(this.sound_slider.progress * 100);
        this.sound_label.string = this.sound_value;
    }

    saveSetting() {
        var newSetting = {
            sound: this.sound_value,
        };
        cc.sys.localStorage.setItem("SettingData", JSON.stringify(newSetting));
        Global.SettingData = newSetting;

        cc.director.loadScene("start_scene");
    }

    change_sound_slider(slider) {
        this.sound_value = parseInt(slider.progress * 100);
        this.sound_label.string = this.sound_value;
    }
    // update (dt) {}
}
