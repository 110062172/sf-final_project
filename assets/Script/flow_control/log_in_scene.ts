// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Button)
    log_in_btn: cc.Button = null;

    @property(cc.EditBox)
    email_input: cc.EditBox = null;

    @property(cc.EditBox)
    password_input: cc.EditBox = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.log_in_btn.node.on("click", this.loaduserScene, this);
    }

    start() {}

    loaduserScene() {
        firebase
            .auth()
            .signInWithEmailAndPassword(this.email_input.string, this.password_input.string)
            .then((res) => {
                var ref = firebase.database().ref("user/" + firebase.auth().currentUser.uid);
                ref.on("value", (res)=>{
                    if(res){
                        Global.Userdata = res.val();
                        cc.director.loadScene("loading_scene");
                    }
                    else{
                        console.log("error");
                    }
                });
                
            })
            .catch(function (error) {
                alert(error.message);
            });

    }
    // update (dt) {}
}
