// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class leader_board extends cc.Component {
    @property(cc.Label)
    a_name_label: cc.Label = null;

    @property(cc.Label)
    b_name_label: cc.Label = null;

    @property(cc.Label)
    c_name_label: cc.Label = null;

    @property(cc.Label)
    d_name_label: cc.Label = null;

    @property(cc.Label)
    e_name_label: cc.Label = null;

    @property(cc.Label)
    a_coin_label: cc.Label = null;

    @property(cc.Label)
    b_coin_label: cc.Label = null;

    @property(cc.Label)
    c_coin_label: cc.Label = null;

    @property(cc.Label)
    d_coin_label: cc.Label = null;

    @property(cc.Label)
    e_coin_label: cc.Label = null;

    @property(cc.Button)
    back_btn: cc.Button = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.back_btn.node.on("click", this.loaduserScene, this);

    }

    start() {
        this.update_leaderboard();
        this.schedule(this.update_leaderboard, 1);
    }

    loaduserScene() {
        cc.director.loadScene("user_scene");
    }

    update_leaderboard() {
        var ref = firebase.database().ref("user/").orderByChild("coin");
        ref.once("value").then((val) => {
            var data = [];
            val.forEach((element) => {
                data.push([element.val().username, element.val().coin]);
            });
            // console.log(data);
            data.reverse();
            Global.leader = data;
        });

        var name_labels = [
            this.a_name_label,
            this.b_name_label,
            this.c_name_label,
            this.d_name_label,
            this.e_name_label,
        ];

        var coin_labels = [
            this.a_coin_label,
            this.b_coin_label,
            this.c_coin_label,
            this.d_coin_label,
            this.e_coin_label,
        ];

        for (var i = 0; i < 5; i++) {
            if (i < Global.leader.length) {
                console.log(Global.leader[i][0]);
                name_labels[i].string = Global.leader[i][0];
                coin_labels[i].string = Global.leader[i][1];
            } else {
                name_labels[i].string = "";
                coin_labels[i].string = "";
            }
        }
    }


    // update (dt) {}
}
