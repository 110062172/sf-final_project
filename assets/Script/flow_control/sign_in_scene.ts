// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
window.Global = {
    Userdata: null,
};
@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Button)
    google_btn: cc.Button = null;

    @property(cc.Button)
    sign_up_btn: cc.Button = null;

    @property(cc.Button)
    log_in_btn: cc.Button = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.log_in_btn.node.on("click", this.loadloginScene, this);
        this.sign_up_btn.node.on("click", this.loadSignupScene, this);
        this.google_btn.node.on("click", this.google_login, this);
    }

    start() {}
    loadloginScene() {
        cc.director.loadScene("log_in_scene");
    }
    loadSignupScene() {
        cc.director.loadScene("sign_up_scene");
    }
    google_login() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase
            .auth()
            .signInWithPopup(provider)
            .then((res) => {
                var ref = firebase.database().ref("user/" + firebase.auth().currentUser.uid);
                ref.on("value", (res)=>{
                    if(!res.val()){
                        console.log("new user");
                        var data = {
                            username:firebase.auth().currentUser.displayName,
                            uid:firebase.auth().currentUser.uid,
                            coin:0
                        };
                        Global.Userdata = data;
                        ref.set(data).then(()=>{
                            cc.director.loadScene("loading_scene");
                        });
                    }
                    else{
                        Global.Userdata = res.val();
                        cc.director.loadScene("loading_scene");
                    }
                });
            })
            .catch(
                console.log("error");
            );
    }
    // update (dt) {}
}
