// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Button)
    sign_up_btn: cc.Button = null;

    @property(cc.EditBox)
    email_input: cc.EditBox = null;

    @property(cc.EditBox)
    password_input: cc.EditBox = null;

    @property(cc.EditBox)
    username_input: cc.EditBox = null;
    // LIFE-CYCLE CALLBACKS:
    @property(cc.AudioClip)
    bgm: cc.AudioClip = null;

    onLoad() {
        this.sign_up_btn.node.on("click", this.loaduserScene, this);
    }

    start() {
        // cc.audioEngine.setMusicVolume(Global.SettingData.sound / 100);
        // cc.audioEngine.playMusic(this.bgm, true);
    }

    loaduserScene() {
        firebase
            .auth()
            .createUserWithEmailAndPassword(
                this.email_input.string,
                this.password_input.string
            )
            .then((res) => {
                if (res) {
                    var ref = firebase.database().ref("user/" + res.user.uid);
                    var data = {
                        username: this.username_input.string,
                        uid: res.user.uid,
                        coin: 0,
                    };
                    Global.Userdata = data;
                    ref.set(data).then(() => {
                        cc.director.loadScene("loading_scene");
                    });
                }
            })
            .catch(function (error) {
                alert(error.message);
            });
        
    }
    // update (dt) {}
}
