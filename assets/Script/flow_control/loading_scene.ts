// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Animation) 
    loading:cc.Animation:null;
    // @property(cc.Node) 
    // loading:cc.Node:null;

    // @property(cc.AnimationClip)
    // loading_aniation:cc.AnimationClip = null;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        let seq = cc.sequence(
            cc.callFunc(() => {
                // this.loading.getComponent(cc.Animation).play("loading");
                this.loading.play("loading");
            }),

            cc.delayTime(3),
            cc.callFunc(() => {
                cc.director.loadScene("user_scene");
            })
        );
        if(Global.Userdata){
            Global.Userdata.heart = 5;
        }
        
        this.node.runAction(seq);
    }

    // update (dt) {}
}
