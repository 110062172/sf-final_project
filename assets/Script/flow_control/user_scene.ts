// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {



    // LIFE-CYCLE CALLBACKS:
    @property(cc.Button)
    sign_btn: cc.Button = null;

    onLoad () {
        this.sign_btn.node.on("click", this.signout, this);
    }

    start () {
        //this.signout();
        //console.log(Global.Userdata);
    }
    // loadLeaderboard() {
    //     var ref = firebase.database().ref("user/").orderByChild("coin");
    //     ref.once("value").then((val) => {
    //         var data = [];
    //         val.forEach((element) => {
    //             data.push([element.val().username, element.val().coin]);
    //         });
    //         // console.log(data);
    //         data.reverse();
    //         Global.leader = data;
    //         cc.director.loadScene("leader_board");
    //     });
    // }
    // update (dt) {}
    signout(){
        firebase.auth().signOut().then(() => {
            console.log("signout");
            
            cc.director.loadScene("start_scene");
        });
    }
}
