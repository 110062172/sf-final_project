import { Monster_prototype } from "./monster_prototype";
import { Player } from "./Player";

const { ccclass, property } = cc._decorator;

@ccclass
export class Monster2 extends Monster_prototype {

    @property(Number)
    health: number = 2;

    @property(cc.ParticleSystem)
    blood_effect: cc.ParticleSystem = null;

    // moving states
    // state0: up ; state1: stall ; state2: down ; state3: stall
    private state = 0;
    private num_states = 4;
    protected easeRate_moving: number = 2;
    protected block_size: number;
    protected moving_time: number;

    // animation
    protected anim: cc.Animation;
    protected anim_state: cc.AnimationState;

    // LIFE-CYCLE CALLBACKS:

    public self_destroy = () => {
        this.anim.stop();
        this.anim_state = this.anim.play("green_slime_die");
        this.anim.on("finished", () => {
            this.node.destroy();
        }, this);
    }

    play_idle = (): cc.AnimationState => {
        return this.anim.play("green_slime_idle");
    }

    public play_attack = () => {

    }

    play_blood_effect = () => {
        this.blood_effect.resetSystem();
    }

    public get_health = () => {
        return this.health;
    }

    public current_pos = () => {
        return new cc.Vec2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y);
    }

    public next_pos = () => {

        if (this.state == 0) {

            return new cc.Vec2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y + this.block_size);
        }
        else if (this.state == 2) {
            return new cc.Vec2(this.node.convertToWorldSpaceAR(cc.v2(0, 0)).x, this.node.convertToWorldSpaceAR(cc.v2(0, 0)).y - this.block_size);
        }

        return this.current_pos();
    }

    public will_move_in_next_state = () => {
        if (this.state == 0 || this.state == 2) {
            return true;
        }
        else {
            return false;
        }
    }

    public attacked = () => {
        this.play_blood_effect();
        --this.health;

        if (this.health <= 0) {
            this.self_destroy();
        }
    }

    go_up = () => {
        //console.log(this.block_size, this.node.y, this.moving_time, this.easeRate_moving);
        let action = cc.moveBy(this.moving_time, 0, this.block_size).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
    }

    go_down = () => {
        let action = cc.moveBy(this.moving_time, 0, -this.block_size).easing(cc.easeInOut(this.easeRate_moving));
        this.node.runAction(action);
    }

    public move = () => {
        if (this.state == 0) {
            this.go_up();
        }
        else if (this.state == 2) {
            this.go_down();
        }

        // change state
        this.state = (this.state + 1) % this.num_states;
    }

    onBeginContact(contact, self, other) {
        // if (other.tag == 6) {
        //     this.attacked();
        // }
    }

    onLoad() {
        // obtain block size
        let player = cc.find("Player").getComponent(Player);
        this.block_size = player.get_block_size();
        this.moving_time = player.get_moving_time();

        // animation
        this.anim = this.getComponent(cc.Animation);
        this.anim_state = this.play_idle();
        this.anim_state.repeatCount = Infinity;
    }

    start() {
    }

    update(dt) {
    }
}
