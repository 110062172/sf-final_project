
const { ccclass, property } = cc._decorator;

@ccclass
export class Arrow extends cc.Component {


    @property(cc.ParticleSystem)
    flame: cc.ParticleSystem = null;

    private anim: cc.Animation = null;
    private anim_state: cc.AnimationState = null;

    // LIFE-CYCLE CALLBACKS:

    play_flame = () => {
        this.flame.resetSystem();
    }

    public go_up = () => {
        this.play_flame();
        this.anim.play("to_up");

        this.anim.on("finished", () => {
            this.node.destroy();
        }, this);
    }

    public go_down = () => {
        this.play_flame();
        this.anim.play("to_bottom");

        this.anim.on("finished", () => {
            this.node.destroy();
        }, this);
    }

    public go_left = () => {
        this.play_flame();
        this.anim.play("to_left");

        this.anim.on("finished", () => {
            this.node.destroy();
        }, this);
    }

    public go_right = () => {
        this.play_flame();
        this.anim.play("to_left");

        this.anim.on("finished", () => {
            this.node.destroy();
        }, this);
        // let action = cc.moveBy(.5, 100, 0).easing(cc.easeInOut(2));
        // this.node.runAction(action);
    }

    onBeginContact(contact, self, other) {
        this.node.destroy();
    }

    onLoad() {

        // animation
        this.anim = this.getComponent(cc.Animation);

    }

    start() {

    }

    // update (dt) {}
}
