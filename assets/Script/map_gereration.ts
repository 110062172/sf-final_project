const { ccclass, property } = cc._decorator;

const ROOM_SIZE = 640;
const TILE_SIZE = 64;
const trimSize = 1;
const dirx = [0, 0, 1, -1];
const diry = [1, -1, 0, 0];

@ccclass
export class DungeonGenerator extends cc.Component {

  @property(cc.Node)
  loadNode: cc.Node = null;

  @property(cc.Node)
  mapNode: cc.Node = null;

  private width: number;
  private height: number;
  private map: number[][];

  onLoad() {
    cc.director.getPhysicsManager().enabled = true;
    cc.director.getCollisionManager().enabled = true;
  }

  generate(): void {
    this.partition(0, 0, this.width, this.height);
    this.connectRooms();
  }
  partition(x1: number, y1: number, x2: number, y2: number) {
    if (x2 - x1 < ROOM_SIZE || y2 - y1 < ROOM_SIZE) {
      this.createRoom(x1, y1, x2, y2);
    } else {
      const flag = Math.random() < 0.55;
      if (flag) {
        const mid = Math.floor(Math.random() * (y2 - y1 + 1) + y1);
        this.partition(x1, y1, x2, mid - 1);
        this.partition(x1, mid + 1, x2, y2);
      } else {
        const mid = Math.floor(Math.random() * (x2 - x1 + 1) + x1);
        this.partition(x1, y1, mid - 1, y2);
        this.partition(mid + 1, y1, x2, y2);
      }
    }
  }

  createRoom(x1: number, y1: number, x2: number, y2: number) {
    
  }
  connectRooms()
  {

  }
}